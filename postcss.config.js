// const cssnano = require("cssnano");
const postcssImport = require("postcss-import");
const postcssPresetEnv = require("postcss-preset-env");
const tailwindcss = require("tailwindcss");
const tailwindcssConfig = require("./tailwind.config");

// const { NODE_ENV } = process.env.NODE_ENV;
// const dev = NODE_ENV === "development";

module.exports = {
  plugins: [
    postcssImport,
    tailwindcss(tailwindcssConfig),

    postcssPresetEnv({
      // Full list of features: https://github.com/csstools/postcss-preset-env/blob/master/src/lib/plugins-by-id.js#L36
      features: {
        "nesting-rules": true, // delete if you don’t want nesting (optional)
      },
    }),

    // Minify if prod
    /* !dev && cssnano({
      preset: ["default", { discardComments: { removeAll: true } }],
    }), */
  ],
};
