import { Content } from "./content.interface";

export class Contact extends Content<number> {
  key?: number;

  first: string;

  last: string;

  constructor(first: string, last: string, key?: number) {
    super();

    this.first = first;
    this.last = last;
    if (key) this.key = key;
  }
}
