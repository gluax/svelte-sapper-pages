import type { Collection } from "dexie";
import type { Writable } from "svelte/store";

import { DBInterface } from "./db.interface";
import { Contact } from "./contact.model";

import { IndexDBStore, createIndexDBStoreKeyValue } from "../stores";

class ContactDB extends DBInterface<Contact, number> {
  contacts: Dexie.Table<Contact, number>;

  async del(key: number): Promise<void> {
    this.contacts.delete(key);
  }

  async get(key: number): Promise<Contact | undefined> {
    const contact = await this.contacts.get(key);

    return contact;
  }

  async insert(content: Contact): Promise<void> {
    const num = await this.contacts.put(content);
    content.key = num;
  }

  async insertMany(content: Array<Contact>): Promise<void> {
    console.log("content", content);
    await this.contacts.bulkPut(content);
  }

  async list(): Promise<Contact[]> {
    return this.contacts.toArray();
  }

  async map(): Promise<Collection<Contact, number> | undefined> {
    return this.contacts.toCollection();
  }

  async update(content: Contact): Promise<boolean> {
    if (!content.key) return false;

    const exists = await this.get(content.key);
    if (!exists) throw new Error("Contact does not exist.");

    await this.contacts.put(content);

    return true;
  }

  constructor() {
    super("ContactDB");

    this.version(1).stores({
      contacts: "++key, first, last",
      kv: "[key],value",
    });

    this.contacts = this.table("contacts");
    this.contacts.mapToClass(Contact);
  }
}

export const ContactStore = (
  async (): Promise<Writable<Array<Contact>>> => IndexDBStore<
  number, Contact, ContactDB>(new ContactDB())
);

export const me = async (): Promise<Writable<Contact>> => createIndexDBStoreKeyValue(
  new Contact("Jon", "Pavlik", 1),
  new ContactDB(),
);
