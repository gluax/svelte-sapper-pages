/* eslint-disable max-classes-per-file */
import Dexie, { Collection } from "dexie";

export class DBInterface<C, K> extends Dexie {
  del(key: K): Promise<void> {
    console.debug("Content delete key:", key);

    return new Promise<void>((resolve) => {
      resolve();
    });
  }

  get(key: K): Promise<C | undefined> {
    console.debug("Content get key:", key);

    return new Promise<C>((resolve) => {
      let content: C | undefined;
      resolve(content);
    });
  }

  insert(content: C): Promise<void> {
    console.debug("Content insert content:", content);

    return new Promise<void>((resolve) => {
      resolve();
    });
  }

  insertMany(content: Array<C>): Promise<void> {
    console.debug("Content insertMany content:", content);

    return new Promise<void>((resolve) => {
      resolve();
    });
  }

  list(): Promise<C[]> {
    console.debug("Content Array.");

    return new Promise<C[]>((resolve) => {
      const dummy: C[] = [];
      resolve(dummy);
    });
  }

  map(): Promise<Collection<C, K> | undefined> {
    console.debug("Content Collection.");

    return new Promise<Collection<C, K>>((resolve) => {
      let dummy: Collection<C, K> | undefined;
      resolve(dummy);
    });
  }

  update(content: C): Promise<boolean> {
    console.debug("Content update content:", content);

    return new Promise<boolean>((resolve) => {
      resolve(false);
    });
  }
}
