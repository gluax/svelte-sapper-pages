import { Content } from "./content.interface";

export class KV extends Content<string> {
  key: string;

  value?: string;

  constructor(key: string, value?: string) {
    super();

    this.key = key;
    if (value) this.value = value;
  }
}
