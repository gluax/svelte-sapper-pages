import * as sapper from "@sapper/server"; // eslint-disable-line import/no-unresolved
import compression from "compression";
import express, { Express } from "express";
import sirv from "sirv";
import { graphqlHTTP } from "express-graphql";

import { schema, root } from "./graphql";

const { APP_NAME, NODE_ENV, PORT } = process.env;
const dev = NODE_ENV === "development";
const APP_PATH = APP_NAME || "/";
const url = dev ? "" : `/${APP_PATH}`;

interface AppSession {
  appPath: string,
  dev: boolean
}

const createServer = async (graphqlPath: string): Promise<Express> => {
  const app = express();
  const basepath = express();

  basepath.use(graphqlPath, graphqlHTTP({
    schema,
    rootValue: root,
    graphiql: true,
  }));

  basepath.use(
    compression({ threshold: 0 }),
    sapper.middleware({
      session: (): AppSession => ({
        appPath: APP_PATH,
        dev,
      }),
    }),
    sirv("static", { dev }),
    sapper.middleware(),
  );

  console.log(url);
  app.use(`${url}`, basepath);

  return app;
};

createServer("/graphql").then((app) => {
  app.listen(PORT, (err?: Error | undefined): void => {
    if (err) console.log("error", err);
  });
});
