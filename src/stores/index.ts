import { createLocalStore } from "./localstorage.store";

export { IndexDBStore, createIndexDBStoreKeyValue } from "./indexDB.store";

export const name = createLocalStore("name");
export const universe = createLocalStore("universe", 42);
