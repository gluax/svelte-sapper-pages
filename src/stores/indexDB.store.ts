import { Writable, writable } from "svelte/store";
import type { Content, DBInterface } from "../models";

type Updater<T> = (value: T) => T;

export async function createIndexDBStoreKeyValue<
                        K,
                        C extends Content<K>,
                        CA extends DBInterface<C, K>
                      >(content: C, caller: CA): Promise<Writable<C>> {
  const { subscribe, set } = writable(content);
  let current = content;

  if (content.key) {
    const exists = await caller.get(content.key);

    if (exists) {
      set(exists);
    } else {
      await caller.insert(content);
      set(content);
    }
  } else {
    await caller.insert(content);
    set(content);
  }

  return {
    subscribe,
    set: async (item: C) => {
      await caller.insert(item);
      set(item);
      current = item;
    },
    update: async (updater: Updater<C>) => {
      updater(current);
      console.log(current);
      current.key = content.key;
      await caller.update(current);
      set(current);
    },
  };
}

export async function IndexDBStore<
                        K,
                        C extends Content<K>,
                        CA extends DBInterface<C, K>
                      >(caller: CA): Promise<Writable<Array<C>>> {
  let current = await caller.list();
  const { subscribe, set } = writable(current);

  return {
    subscribe,
    set: async (item: Array<C>) => {
      await caller.insertMany(item);
      set(item);
      current = item;
    },
    update: async (updater: Updater<Array<C>>) => {
      updater(current);
      console.log(current);
      await caller.insertMany(current);
      set(current);
    },
  };
}
