import { writable, Writable } from "svelte/store";

interface LocalStorageStore<T> extends Writable<T> {
  useLocalStorage: () => void;
}

export function createLocalStore(key: string, json?: unknown): LocalStorageStore<unknown> {
  const { subscribe, set, update } = writable(json);

  return {
    subscribe,
    set,
    update,
    useLocalStorage: (): void => {
      const data = localStorage.getItem(key);

      if (data && data !== "undefined") set(JSON.parse(data));

      subscribe((value) => {
        localStorage.setItem(key, JSON.stringify(value));
      });
    },
  };
}
