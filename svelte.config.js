const sveltePreprocess = require("svelte-preprocess");

const createPreprocessors = ({ sourceMap }) => [
  sveltePreprocess({
    sourceMap,
    postcss: true,
    defaults: {
      script: "typescript",
      style: "postcss",
    },
  }),
  // You could have more preprocessors, like mdsvex
];

module.exports = {
  createPreprocessors,
  // Options for `svelte-check` and the VS Code extension
  preprocess: createPreprocessors({ sourceMap: true }),
};
