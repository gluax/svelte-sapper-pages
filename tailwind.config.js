const tailwindui = require("@tailwindcss/ui");
const typography = require("@tailwindcss/typography");

module.exports = {
  experimental: {
    // uniformColorPalette: true,
    // extendedFontSizeScale: true,
    // currently Sapper dev server chokes on this
    // applyComplexClasses: true,
  },

  purge: {
    // needs to be set if we want to purge all unused
    // @tailwind/typography styles
    options: {
      defaultExtractor: (content) => [...content.matchAll(/(?:class:)*([\w\d-/:%.]+)/gm)].map(([_match, group, ..._rest]) => group),
      keyframes: true,
    },

    content: ["./src/**/*.svelte", "./src/**/*.html"],
  },

  theme: {
    extend: {},
  },

  variants: {},

  plugins: [
    tailwindui,
    typography,
  ],

  future: {
    purgeLayersByDefault: true,
    removeDeprecatedGapUtilities: true,
  },
};
